#

## Background

Shorting an asset is the process where you borrow the asset from an exchange in order to sell it immediately. 

This is under the expectation that the asset will drop in value, so you can then purchase the asset back  for less than you sold it previously to return to the exchange at a later point in time in order to profit from the difference in value between your initial sell price and the price at which you buy the asset back.


# Longing

This is the process where you borrow funds from the exchange to purchase the asset and then sell it when the price has increased.

The difference between your initial buy price and the price at which you sell the asset is your profit.


# Leveraged trading
Using a more stable asset as collateral, exchanges will allow you to borrow multiples of the value of your collateral, so multiple your profits (or losses)

# Indexes
An index is a basket of assets that allows exposure to many different instruments withint the same asset type. This allows you to diversify without the effort of actually finding all the individual tickers.


# Market Market making

On a single market, you would look to buy buy and sell simultaneous a certain position away from the current price.

For example, if the price of BTC is 100, you would enter a buy order at 95 and a sell order at 105

If the price of btc raises to 105, your sell order is matched, so you would then enter another sell order at 110, and you would additionally enter another buy order at 100.

You would now have a net short position. This is what is known as a Short side biases market maker, as you would set a defined amount of BTC by which you would be short.


If the price of btc then raises to 110, you would do the same again, eventually the price of btc will drop and hit your buy orders, thus locking in a profit for the most recent trade.

The inverse is also true, in that if you enter the buy side first, then you have a buy side bias

![](./mm_example.png)


# The concept
If you enter both a long and a short position, on 2 correlated assets, you can hedge your risk that the trade will move against you.

By implementing the market making algorithm on both buy and sell side, for the 2 assets, you can basically enter into a overall position where the sell side bias is covering the long side, so if there is a overall drop in the markets, your buy side bias market will be losing money, whilst your sell side bias will be making money.

# The problem
There are 2 correlated assets;
BTC and and index call SHITCOIN (this is an index of all of the crypto assets weighted by market cap excluding BTC)

I know the 2 are correlated, and I also know that one of the assets moves more dramatically that the other.

![](./assets.png)


I am looking to know how I can caluclate what the ratios of exposure should be to remain in a hedged position based on the window of data.


# Data

I have provided OHLC for both of the assets.


# How i would solve it

1. normalised the changes between each close
2. create 100_000 different different portfolios with different weights.
3. then for each porftolio, I would calculate for each step what the value was
4. I would then choose the portfolio with a average value closest to 1


# more elegant entries;
Additionally, a sudo code of something similar is below, (this works well btw)

```
for each portfolio:
    for each step:
        calculate SD of the differences between the two for last 50 steps.
            if difference between is > 2 sd:
                enter long short
    if final value > best:
        best = portfolio
```
